# README.md

Welcome to the ZnD repo! Here's a few things to get you started.

[[_TOC_]]


# Useful locations

Some useful locations across the Zora no Densha community:

- The Zora no Densha [Wiki](https://gitlab.com/ZnDevelopment/Zora-no-Densha/-/wikis/home "Zora no Densha Wiki") contains tutorials, recipes, and documentation.
- Our [license](LICENSE.md) regulates what may be done with the source code of Zora no Densha.
- You can get in touch with us on the official Zora no Densha [Discord](https://discord.gg/s6zvDV8 "Zora no Densha Discord invitation").
- Bugs, crashes, and suggestions can be reported either on our Discord (see above) or on our [GitLab issues](https://gitlab.com/ZnDevelopment/Zora-no-Densha/-/issues) page.


# Guidelines for developers

We have a few guidelines (implicit and explicit) to keep things organised around here.  
Please note that these guidelines are constantly developed and my be subject to change. Make sure to check them out regularly.

## Submitting code changes

In order to start working on the Zora no Densha source code, please checkout your own branch from either available `mc-X.Y.Z` branch.
It's highly recommended to prepend the name of the branch you checked out to the name you wish to give your branch.

If you are new to Git, make sure to read about [merge requests](https://docs.gitlab.com/ee/topics/gitlab_flow.html#mergepull-requests-with-gitlab-flow).

Once you are done and satisfied with what you've been working on, you may want to integrate your changes to the main repository.
Ensure your changes run without issues by thoroughly **testing** them first. After you ensured there are no issues coming from your code,
create a merge request to merge your branch back into the official branch it originated from.

Merge requests need approval of at least two (2) project members before being executed. This means that we review your code changes and may get in touch with you if necessary.


## Building Zora no Densha releases

Tagged commits on protected branches (such as `master` or `mc-X.Y.Z`) are primary build targets.
These tags follow Forge's [versioning scheme](https://mcforge.readthedocs.io/en/latest/conventions/versioning/ "Minecraft Forge - Versioning") and their corresponding builds should carry the exact same name as their tag, prepended by the mod name.
This means a mod file built from tag `1.7.10-0.9.0.1` should carry the name `Zora no Densha 1.7.10-0.9.0.1`.
